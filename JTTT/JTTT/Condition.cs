﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public abstract class Condition
    {
        public int ConditionID { get; set; }
        public string address { get; set; }
        public string tag { get; set; }

        abstract public string GetResult(string filename);

        public class WeatherCondition : Condition
        {
            public WeatherCondition() { }

            public WeatherCondition(string miasto, string mintemperatura)
            {
                this.address = miasto;
                this.tag = mintemperatura;
            }

            public override string GetResult(string filename)
            {
                WebClient web_client = new WebClient();
                string link = "http://api.openweathermap.org/data/2.5/weather?q=" + this.address + ",pl&APPID=4318fa33ec64d2108bbde5e645e8b3fc";
                var wynik = web_client.DownloadString(link);
                var dane = JsonConvert.DeserializeObject<Pogoda.RootObject>(wynik);

                Pogoda.RootObject info = dane;
                string tekst_wyswietlany = $"W mieście {dane.name} jest {dane.main.temp - 273.15} stopni.\n Ciśnienie {dane.main.pressure} hPa.";
                web_client.DownloadFile("http://openweathermap.org/img/w/" + dane.weather[0].icon + ".png", "file.jpg");


                return tekst_wyswietlany;
            }


        }

        public class ImageCondition : Condition
        {
            public ImageCondition() { }

            public ImageCondition(string url, string fraza)
            {
                this.address = url;
                this.tag = fraza;
            }

            public override string GetResult(string filename)
            {
                string result = "";

                HTML html = new HTML(address, tag);
                html.DownloadImageURL();
                result = this.tag;
                
                return result;
            }

        }
    }
}
