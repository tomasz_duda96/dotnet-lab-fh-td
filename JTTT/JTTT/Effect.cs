﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public abstract class Effect
    {
        public int EffectID { get; set; }
        virtual public void GetResult(string data, string filepath, string address) { }
    }

    public class DisplayEffect : Effect
    {
        public DisplayEffect() { }

        public override void GetResult(string message, string filepath, string address)
        {
            FormActionDisplay window = new FormActionDisplay(message, "file.jpg"); 

            window.ShowDialog();
        }
    }

    public class SendMailEffect : Effect
    {
        public string adress;

        public SendMailEffect() { }
        
        public SendMailEffect(string address)
        {
            adress = address;
        }


        public override void GetResult(string body, string subject, string address) 
        {
            Email mail = new Email("FHTDtest@gmail.com", address, body, subject);
            mail.SendMail();
        }

    }

}

