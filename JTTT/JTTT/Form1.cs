﻿using System;
using System.ComponentModel;
using System.Net; // web client
using System.Net.Mail; // smtp client
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Data.Entity;
using Newtonsoft.Json;



namespace JTTT
{

    public partial class Form1 : Form
    {
        BindingList<Task> lista;
        public Form1()
        {
            InitializeComponent();
            lista = new BindingList<Task>();
            poleURL.Text = "https://demotywatory.pl/";
            poleEmail.Text = "FHTDtest@gmail.com";
            listBox1.DataSource = lista;
            tabPage1.Text = "Sprawdź stronę";
            tabPage2.Text = "Sprawdź pogodę";
            tabPage3.Text = "Wyslij maila";
            tabPage4.Text = "Pokaż komunikat";


            if (Database.Exists("ZadankoUlepszone2"))
            {
                using (var db = new ZadaniaDbContext())
                {
                    foreach (var zad in db.Zadania.Include("TaskCondition").Include("TaskEffect"))
                    {
                        lista.Add(zad);
                    }
                }
            }
            else
            {
                MessageBox.Show("Nie odnaleziono bazy danych - niczego nie wczytano.");
            }
        }


        private void PrzyciskDodaj(object sender, EventArgs e)
        {
            Task newTask = new Task();


            if (tabControl1.SelectedTab == tabPage1 && tabControl2.SelectedTab == tabPage3)
            {

                Condition.ImageCondition warunek = new Condition.ImageCondition(poleURL.Text, poleFraza.Text);
                SendMailEffect mailEffect = new SendMailEffect(poleEmail.Text);

                newTask.TaskCondition = warunek;
                newTask.TaskEffect = mailEffect;
                newTask.mail = poleEmail.Text;
            }

            if (tabControl1.SelectedTab == tabPage2 && tabControl2.SelectedTab == tabPage3)
            {
                Condition.WeatherCondition warunek = new Condition.WeatherCondition(textBox1.Text, textBox2.Text);
                SendMailEffect mailEffect = new SendMailEffect(poleEmail.Text);
                newTask.TaskCondition = warunek;
                newTask.TaskEffect = mailEffect;
                newTask.mail = poleEmail.Text;
                newTask.temp = Convert.ToDouble(textBox2.Text);

            }

            if (tabControl1.SelectedTab == tabPage1 && tabControl2.SelectedTab == tabPage4)
            {
                Condition.ImageCondition warunek = new Condition.ImageCondition(poleURL.Text, poleFraza.Text);
                DisplayEffect wyswietl = new DisplayEffect();
                newTask.TaskCondition = warunek;
                newTask.TaskEffect = wyswietl;
                newTask.mail = poleEmail.Text;
            }

            if (tabControl1.SelectedTab == tabPage2 && tabControl2.SelectedTab == tabPage4)
            {
                Condition.WeatherCondition warunek = new Condition.WeatherCondition(textBox1.Text, textBox2.Text);
                DisplayEffect wyswietl = new DisplayEffect();
                newTask.TaskCondition = warunek;
                newTask.TaskEffect = wyswietl;
                newTask.mail = poleEmail.Text;
                newTask.temp = Convert.ToDouble(textBox2.Text);
            }

            using (var temp = new ZadaniaDbContext())
            {
                lista.Add(newTask);
                temp.Zadania.Add(newTask);
                temp.SaveChanges();
            }
        }

        private void PrzyciskWykonaj(object sender, EventArgs e)
        {
            foreach (var element in lista)
            {
                element.ExecuteTask();
            }
            MessageBox.Show("Wykonano wszystko pod guzikiem wykonaj!");
        }

        private void PrzyciskCzysc(object sender, EventArgs e)
        {
            while (lista.Count > 0)
            {
                lista.Remove(lista[0]);
            }
            using (var temp = new ZadaniaDbContext())
            {
                temp.Database.ExecuteSqlCommand("DELETE FROM Tasks WHERE 1=1;");
                temp.Database.ExecuteSqlCommand("DELETE FROM Effects WHERE 1=1;");
                temp.Database.ExecuteSqlCommand("DELETE FROM Conditions WHERE 1=1;");
                temp.SaveChanges();
            }

        }


        private void PrzyciskDeserialize(object sender, EventArgs e)
        {
            lista = Serialization.Deserialize();
            listBox1.DataSource = lista;
        }

        private void PrzyciskSerialize(object sender, EventArgs e)
        {
            Serialization.Serialize(lista);
        }

    }
}