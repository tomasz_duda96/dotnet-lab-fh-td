﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace JTTT
{
    public partial class FormActionDisplay : Form
    {
        private string filepath;

        public FormActionDisplay()
        {
            InitializeComponent();
        }


        public FormActionDisplay(string message, string filepath)
        {
            InitializeComponent();
            this.filepath = "file.jpg";
            textBox1.Text = message;
            pictureBox1.ImageLocation = filepath;
            pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
        }

        public FormActionDisplay(string url, string fraza, bool dummy)
        {
            InitializeComponent();
            var webClient = new WebClient();
            HTML temp = new HTML(url, fraza);
            temp.DownloadImageURL();
            this.filepath = temp.filepath;
           
            pictureBox1.ImageLocation = filepath;
            pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
            
        }
        
    }
}
