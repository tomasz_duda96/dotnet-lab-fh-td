﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Windows.Forms;

namespace JTTT
{
    public class HTML
    {

        string _HTML;
        string _fraza;
        public string filename;
        public string filepath;

        public HTML(string url, string fraza)
        {
            SetPageHtml(url);
            _fraza = fraza;
        }

        public void SetPageHtml(string url)
        {
            WebClient web_client = new WebClient();
            web_client.Encoding = Encoding.UTF8;
            _HTML = WebUtility.HtmlDecode(web_client.DownloadString(url));
        } 

        public bool DownloadImageURL()
        {
            string link;
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(_HTML);

            var nodes = doc.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {
                string tekstAlt = node.GetAttributeValue("alt", "");
                if (tekstAlt.ToLower().Contains(_fraza.ToLower()))
                {
                    Console.WriteLine("Src val:" + node.GetAttributeValue("src", ""));
                    link = node.GetAttributeValue("src", "");

                    using (WebClient webClient = new WebClient())
                    {
                        if (!link.Contains("https://"))
                            link = "https://img2.demotywatoryfb.pl/" + link;

                    
                        webClient.DownloadFile(link, "file.jpg");
                        string dir = Path.GetDirectoryName(Application.ExecutablePath);
                        filename = Path.Combine(dir, "file.jpg");

                        Console.WriteLine(link);
                        filepath = link;
                        return true;
                    }
                }

            }
            return false;
        }

    }
}
