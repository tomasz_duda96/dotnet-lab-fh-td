﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    class Serialization
    {
        public static void Serialize(BindingList<Task> list)
        {
            FileStream file = new FileStream("ListData.dat", FileMode.Create);
            BinaryFormatter form = new BinaryFormatter();
            form.Serialize(file, list);
            file.Close();
        }

        public static BindingList<Task> Deserialize()
        {
            BindingList<Task> list = null;
            FileStream file = new FileStream("ListData.dat", FileMode.Open);
            BinaryFormatter form = new BinaryFormatter();
            list = (BindingList<Task>)form.Deserialize(file);
            file.Close();
            return list;
        }
    }
}
