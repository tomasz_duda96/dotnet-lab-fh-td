﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    [Serializable]
    class Task
    {
        public int taskId { get; set; }
        public int? ConditionID { get; set; }
        public int? EffectID { get; set; }


        [ForeignKey("ConditionID")]
        public virtual Condition TaskCondition {get; set; }
        [ForeignKey("EffectID")]
        public virtual Effect TaskEffect { get; set; }
        public string mail { get; set; }
        public double temp { get; set; }


        public Task() { }
        public Task(Condition cond, Effect eff)
        {
            TaskCondition = cond;
            TaskEffect = eff;
            temp = 100.0;

        }

        public void ExecuteTask()
        {
            var tekst = TaskCondition.GetResult("file.jpg");
            if (tekst.Contains("stopni"))
            {
                int from = tekst.IndexOf("jest ") + "jest ".Length;
                int to = tekst.LastIndexOf("stopni");
                string result = tekst.Substring(from, to-from);
                double result2 = Convert.ToDouble(result);
                Console.WriteLine(result2);
                if (result2 > temp)
                {
                    TaskEffect.GetResult(tekst, "Super gołomp pocztowy", mail);
                }
                else
                {
                    MessageBox.Show("Pogoda w mieście jest niższa niż ustalona granica. Nie wykonuję akcji!");
                }
            }
            else
            {
                TaskEffect.GetResult(tekst, "Super gołomp pocztowy", mail);
            }
        }

    }
    
}
