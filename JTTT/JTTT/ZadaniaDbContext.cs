﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace JTTT
{
    class ZadaniaDbContext : DbContext
    {
        public ZadaniaDbContext() : base("ZadankoUlepszone3") { }

        public DbSet<Task> Zadania { get; set; }
        public DbSet<Effect> Effect { get; set; }
        public DbSet<Condition> Condition { get; set; }
    }
}
